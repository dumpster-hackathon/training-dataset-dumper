/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGGER_BTAG_MATCHER_ALG_H
#define TRIGGER_BTAG_MATCHER_ALG_H

#include "VariableMule.hh"

#include "xAODBTagging/BTaggingContainer.h"
#include "xAODJet/JetContainer.h"

#include "AthenaBaseComps/AthReentrantAlgorithm.h"



class TriggerBTagMatcherAlg: public AthReentrantAlgorithm
{
public:
  TriggerBTagMatcherAlg(const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute (const EventContext&) const override;
  virtual StatusCode finalize () override;
private:
  using JL = ElementLink<xAOD::JetContainer>;
  using BC = xAOD::BTaggingContainer;
  using JC = xAOD::JetContainer;
  std::string m_offlineBtagKey;
  std::string m_triggerBtagKey;
  VariableMule<float, BC> m_floats{NAN};
  VariableMule<int, BC> m_ints{-1};
  VariableMule<int, JC> m_jetInts{-1};
  SG::ReadHandleKey<JC> m_triggerJetKey;
  SG::ReadHandleKey<JC> m_offlineJetKey;
  SG::ReadDecorHandleKey<BC> m_triggerBtag;
  SG::ReadDecorHandleKey<BC> m_offlineBtag;
  SG::WriteDecorHandleKey<BC> m_drDecorator;
  SG::WriteDecorHandleKey<BC> m_dPtDecorator;
};

#endif
