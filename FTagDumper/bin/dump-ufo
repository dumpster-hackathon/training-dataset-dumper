#!/usr/bin/env python

"""
Re run track association for UFO jets to get dR tracks
"""

import sys

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleAssociationAlgCfg
from FlavorTagDiscriminants.FoldDecoratorConfig import FoldDecoratorCfg


from FTagDumper import dumper


def deltaRTracks(
        flags,
        jet_collection,
        track_collection="InDetTrackParticles"
        ):
    '''
    Function to associate deltaR tracks to UFO jets and calling the collection DeltaRTrackLink
    '''

    acc = ComponentAccumulator()

    acc.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=jet_collection,
        InputParticleCollection=track_collection,
        OutputParticleDecoration="DeltaRTrackLink",
    ))

    acc.merge(FoldDecoratorCfg(flags, jetCollection=jet_collection))

    variableRemapping = {
        'BTagTrackToJetAssociator': "DeltaRTrackLink",
        **{f'GN2v01_p{x}': f'dRGN2v01_p{x}' for x in ['u', 'c', 'b', 'tau']}
    }

    nn_base = 'BTagging/20231205/GN2v01/antikt4empflow'
    pf_nns = [f'{nn_base}/network_fold{n}.onnx' for n in range(4)]
    acc.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.JetTagDecoratorAlg(
            'mydRGN2',
            container=jet_collection,
            constituentContainer=track_collection,
            decorator=CompFactory.FlavorTagDiscriminants.MultifoldGNNTool(
                'mydRGN2_tool',
                foldHashName='jetFoldHash',
                nnFiles=pf_nns,
                variableRemapping=variableRemapping,
                # note that the tracks are associated to the jet as
                # and IParticle container.
                trackLinkType='IPARTICLE',
            ),
        )
    )

    variableRemapping = {
        'BTagTrackToJetAssociator': "GhostTrack",
        **{f'GN2v01_p{x}': f'GAGN2v01_p{x}' for x in ['u', 'c', 'b', 'tau']}
    }
    acc.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.JetTagDecoratorAlg(
            'myGAGN2',
            container=jet_collection,
            constituentContainer="GhostTrack",
            decorator=CompFactory.FlavorTagDiscriminants.MultifoldGNNTool(
                'myGAGN2_tool',
                foldHashName='jetFoldHash',
                nnFiles=pf_nns,
                variableRemapping=variableRemapping,
                # note that the tracks are associated to the jet as
                # and IParticle container.
                trackLinkType='IPARTICLE',
            ),
        )
    )


    return acc



def run():

    args = dumper.base_parser(__doc__).parse_args()

    flags = dumper.update_flags(args)
    flags.lock()

    ca = getConfig(flags)
    ca.merge(PoolReadCfg(flags))

    config = dumper.combinedConfig(args.config_file)
    jet_collection = config['jet_collection']

    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # Assicoation of deltaR tracks to the UFO jet
    ca.merge(
        deltaRTracks(
            flags,
            track_collection="InDetTrackParticles",
            jet_collection=jet_collection,
        )
    )

    ca.merge(dumper.getDumperConfig(args))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
