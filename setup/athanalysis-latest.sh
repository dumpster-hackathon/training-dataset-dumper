SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
source ${SCRIPT_PATH%/*}/deconda.sh

echo "=== running setupATLAS ==="
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

echo "=== running asetup ==="
asetup AthAnalysis,main,latest
source ${SCRIPT_PATH%/*}/allow-breaking-edm.sh
